# mypy stubs for the PyQt6 framework

![mypy logo](http://mypy-lang.org/static/mypy_light.svg){width=300}

These are custom stubs for PyQt6 framework. They are based on original
files bundled inside PyQt6 wheel, with additional modifications made,
as bundled stubs often are lacking or incorrect, being auto-generated code.

**Note!** Stubs may be not fully correct, as it has been tested mainly against the
[accwidgets](http://acc-py-repo.cern.ch/browse/project/accwidgets) project. If something
is insufficient for your use case, please contact the maintainers to fix the stubs.

This project is inspired by another
[PyQt6-stubs](https://github.com/python-qt-tools/PyQt6-stubs) effort,
however heavily simplified, and learning from the experience making
additional changes to [PyQt5-stubs](https://gitlab.cern.ch/isinkare/pyqt5-stubs).

If you wish to understand what differences are made compared to the original stubs,
inspect the commit history of this project.

## How to use with mypy

Unlike PyQt5, which shipped `*.pyi` that were invisible to mypy, due to the
absence of `py.typed`, PyQt6 started shipping the marker file, so it is now usable
by mypy. However, many issues still remain, though progress is evident, as original
stubs for PyQt6 6.6 are much better than PyQt6 6.2, for instance.

Because of the bundled `py.typed` marker, mypy always prefers original stubs over
this package. In order to make mypy ignore originals, this package provides a
mypy plugin, that will disable the `py.typed` marker.

Modify your mypy settings like so (e.g. in `pyproject.toml`):

```toml
[tool.mypy]
plugins = [
    "acc_py_pyqt6_mypy_plugin",
]
```

When `py.typed` marker of PyQt6 is detected, it will print a message in the mypy
output about moving it.
