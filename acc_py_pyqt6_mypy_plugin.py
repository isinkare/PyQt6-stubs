from __future__ import annotations

import atexit
import functools
import logging
import pathlib
import site
import typing

import mypy.plugin

PROJECT_ROOT = pathlib.Path(__file__).parent.absolute()


logger = logging.getLogger(__name__)
placements: typing.Dict[pathlib.Path, pathlib.Path] = {}


@functools.lru_cache(maxsize=1)
def disable_type_marker(*pkg_names: str) -> None:
    for user_site in site.getsitepackages():
        if user_site == site.getusersitepackages():
            continue
        logger.debug(f"Inspecting {user_site} for {pkg_names}")
        for pkg_name in pkg_names:
            pkg_dir = pathlib.Path(user_site) / pkg_name
            if pkg_dir.exists() and pkg_dir.is_dir():
                marker = pkg_dir / "py.typed"
                if marker.exists() and marker.is_file():
                    new_marker = pkg_dir / "py.typed.bak"
                    placements[marker] = new_marker
    for prev, new in placements.items():
        logger.info(f"Moving {prev} -> {new}")
        prev.rename(new)


def restore_type_marker() -> None:
    for prev, new in placements.items():
        try:
            new.rename(prev)
        except FileNotFoundError:
            pass
        else:
            logger.info(f"Returning {new} -> {prev}")


atexit.register(restore_type_marker)


class PyQt6StubsPlugin(mypy.plugin.Plugin):
    """This is a placeholder class just to return something to conform to mypy interface."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        disable_type_marker("PyQt6")

    def __del__(self) -> None:
        restore_type_marker()


def plugin(version: str) -> typing.Type[mypy.plugin.Plugin]:
    # ignore version argument if the plugin works with all mypy versions.
    _ = version
    # We run it here, because the plugin instance concerns hooks for specific files,
    # and symbols. Hence, sometimes the generation happens too late, we need to run
    # this code before the any transformation.
    logger.setLevel(logging.INFO)
    h = logging.StreamHandler()
    h.setFormatter(logging.Formatter("PYQT6-PLUGIN: %(message)s"))
    logger.addHandler(h)
    return PyQt6StubsPlugin
