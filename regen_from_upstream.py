import argparse
import logging
import pathlib
import shutil
import subprocess
import sys
import tempfile
import typing
import zipfile

logger = logging.getLogger(__name__)


def download_wheels(download_dir_path: pathlib.Path, wheels: typing.Mapping[str, str]) -> None:
    logger.info(f'Downloading wheels {list(wheels.keys())}')
    reqs = (f"{name}=={ver}" for name, ver in wheels.items())
    subprocess.check_call([
        sys.executable, '-m', 'pip', 'download',
        '-d', download_dir_path,
        '--only-binary', '":all:"',
        *reqs,
    ])


def unpack_original_stubs(wheel_path: pathlib.Path, destination_dir_path: pathlib.Path) -> None:
    logger.info(f'Extracting *.pyi from {wheel_path} to {destination_dir_path}')
    with zipfile.ZipFile(wheel_path, mode='r') as wheel:
        wheel.extractall(
            path=destination_dir_path,
            members=[m for m in wheel.namelist() if m.endswith('.pyi')],
        )


def unpack_uic_code(wheel_path: pathlib.Path, destination_dir_path: pathlib.Path, pyqt_version: str) -> None:
    pyqt_major_ver = pyqt_version.split('.')[0]
    prefix = str(pathlib.Path(f'PyQt{pyqt_major_ver}') / 'uic')
    logger.info(f'Extracting uic code from {wheel_path} to {destination_dir_path}')
    with zipfile.ZipFile(wheel_path, mode='r') as wheel:
        wheel.extractall(
            path=destination_dir_path,
            members=[
                m for m
                in wheel.namelist()
                if m.startswith(prefix)
                and m.endswith('.py')
                # skip qaxcontainer.py, qscintilla.py, etc
                and pathlib.Path(m).parent.name != 'widget-plugins'
            ],
        )


def generate_uic_stubs(source_path: pathlib.Path, destination_dir_path: pathlib.Path) -> None:
    uic_files = source_path.rglob('*.py')
    logger.info(f'Generating stubs from {source_path} into {destination_dir_path}')
    subprocess.check_call([
        'stubgen',
        '--output', destination_dir_path,
        *(str(f) for f in uic_files),
    ])


def move_files(source_path: pathlib.Path, destination_dir_path: pathlib.Path) -> None:
    logger.info(f'Moving files from {source_path} into {destination_dir_path}')
    shutil.copytree(
        src=str(source_path),
        dst=str(destination_dir_path),
        dirs_exist_ok=True,
    )


def apply_manual_patches() -> None:
    patch_path = pathlib.Path(__file__).parent / 'patches'
    logger.info(f'Applying patches from {patch_path}')
    for patch in sorted(patch_path.glob('*.patch')):
        logger.info(f'Applying {patch.name}')
        subprocess.check_call([
            'git',
            'apply',
            '--verbose',
            patch,
        ])


def modify_stubs_with_linters(source_path: pathlib.Path, settings_path: pathlib.Path) -> None:
    logger.info(f'Running isort over {source_path}')
    subprocess.check_call([
        'isort',
        '--settings-path', settings_path,
        source_path,
    ])
    logger.info(f'Running black over {source_path}')
    subprocess.check_call([
        'black',
        '--config', settings_path,
        source_path,
    ])


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-v', '--version',
        help='PyQt package version to pull original stubs from',
        metavar='VERSION',
        default='6.6.1',
    )
    parser.add_argument(
        '-q', '--qsci-version',
        help='PyQt6-Qscintilla package version to pull original stubs from',
        metavar='VERSION',
        default='2.14.1',
    )
    args = parser.parse_args()
    return args


def main() -> None:
    args = parse_args()
    logging.basicConfig(level=logging.INFO)
    # Has to be hardcoded, otherwise git patch files won't apply
    output_path = pathlib.Path(__file__).parent.absolute() / 'PyQt6-stubs'
    logger.info(f'Preparing destination {output_path}')
    shutil.rmtree(output_path, ignore_errors=True)
    output_path.mkdir(exist_ok=True)
    settings_file = pathlib.Path(__file__).parent / 'pyproject.toml'

    with tempfile.TemporaryDirectory() as tmp:
        tmp_path = pathlib.Path(tmp)
        pyqt_major_ver = args.version.split('.')[0]
        wheels = {
            f'PyQt{pyqt_major_ver}': args.version,
            f'PyQt{pyqt_major_ver}-QScintilla': args.qsci_version,
        }
        download_wheels(
            download_dir_path=tmp_path,
            wheels=wheels,
        )
        pyqt_wheel_path = next(iter(tmp_path.rglob(f'*PyQt{pyqt_major_ver}-{args.version}-*.whl')))
        qsci_wheel_path = next(iter(tmp_path.rglob(f'*PyQt{pyqt_major_ver}_QScintilla-{args.qsci_version}-*.whl')))
        temp_src_path = tmp_path / 'src'
        uic_src_path = temp_src_path / 'uic'
        uic_src_path.mkdir(exist_ok=True, parents=True)
        unpack_original_stubs(
            wheel_path=pyqt_wheel_path,
            destination_dir_path=temp_src_path,
        )
        unpack_original_stubs(
            wheel_path=qsci_wheel_path,
            destination_dir_path=temp_src_path,
        )
        move_files(
            source_path=temp_src_path / f'PyQt{pyqt_major_ver}',
            destination_dir_path=output_path,
        )
        unpack_uic_code(
            wheel_path=pyqt_wheel_path,
            destination_dir_path=uic_src_path,
            pyqt_version=args.version,
        )
        generate_uic_stubs(
            source_path=uic_src_path,
            destination_dir_path=output_path,
        )
    modify_stubs_with_linters(output_path, settings_path=settings_file)
    apply_manual_patches()

    # Reformat in case patches have changed something
    modify_stubs_with_linters(output_path, settings_path=settings_file)


if __name__ == '__main__':
    main()
