import pathlib
import subprocess
import sys

import pytest

here = pathlib.Path(__file__).parent
sample_files = list(here.glob("[!test_]*.py"))
stub_dir = here.parent / "PyQt6-stubs"


@pytest.mark.parametrize(
    "file_path",
    [*sample_files, stub_dir],
    ids=[v.name for v in [*sample_files, stub_dir]],
)
def test_mypy(
        file_path: pathlib.Path,
) -> None:
    try:
        output = subprocess.check_output([
            "mypy",
            file_path,
        ], stderr=sys.stdout).decode()
    except subprocess.CalledProcessError as e:
        pytest.fail(e.output.decode(), pytrace=False)
    assert output.startswith("Success: no issues found")


@pytest.mark.parametrize(
    "file_path",
    sample_files,
    ids=[v.name for v in sample_files],
)
def test_example_files_are_runnable(
        file_path: pathlib.Path,
) -> None:
    code = file_path.read_text(encoding="utf-8")
    compiled = compile(code, file_path, "exec")
    exec(compiled, {})
