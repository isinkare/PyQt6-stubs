import io
import typing

from .Compiler import compiler as compiler

def compileUiDir(dir: str, recurse: bool = ..., map: typing.Callable[[str, str], typing.Tuple[str, str]] = ..., max_workers: int = ..., execute: bool = ..., indent: int = ...) -> None: ...
def compileUi(uifile: typing.Union[str, io.FileIO], pyfile: io.FileIO, execute: bool = ..., indent: int = ...) -> None: ...
