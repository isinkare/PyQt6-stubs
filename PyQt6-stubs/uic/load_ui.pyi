import io
import os
import typing

from PyQt6 import QtCore

def loadUiType(uifile: typing.Union[str, io.FileIO]): ...
def loadUi(uifile: typing.Union[os.PathLike, str], baseinstance: typing.Optional[QtCore.QObject] = ..., package: str = ...): ...
