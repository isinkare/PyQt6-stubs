import io
import typing
import xml.etree.ElementTree

class UIFile:
    button_groups: xml.etree.ElementTree.Element
    connections: xml.etree.ElementTree.Element
    custom_widgets: xml.etree.ElementTree.Element
    layout_default: xml.etree.ElementTree.Element
    tab_stops: xml.etree.ElementTree.Element
    widget: xml.etree.ElementTree.Element
    class_name: str
    def __init__(self, ui_file: typing.Union[str, io.FileIO]) -> None: ...
