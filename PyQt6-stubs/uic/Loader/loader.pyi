import io
import typing

from PyQt6 import QtWidgets

from ..uiparser import UIParser as UIParser
from .qobjectcreator import LoaderCreatorPolicy as LoaderCreatorPolicy

class DynamicUILoader(UIParser):
    def __init__(self, package: str) -> None: ...
    def createToplevelWidget(self, classname: str, widgetname: str) -> QtWidgets.QWidget: ...
    toplevelInst: QtWidgets.QWidget
    def loadUi(self, filename: typing.Union[str, io.FileIO], toplevelInst: QtWidgets.QWidget) -> typing.Optional[QtWidgets.QWidget]: ...
